Carolyn Palaga and her team believe patients with hearing loss deserve audiology services based on the highest scientific standards. The team is committed to raising the level of integrity in the field of audiology by practicing evidence-based audiology.

Address: 2076 Granite Avenue, Merritt, BC V1K 1B8, Canada

Phone: 250-315-9688

Website: https://sahaliprohearingservices.com
